bindep - Binary dependency management
=====================================

A utility for declaring what dependencies programs have in a programmatic
manner.

Contents:

.. toctree::
   :maxdepth: 2

   readme
   news
   installation
   usage
   contributing
   releasenotes

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
